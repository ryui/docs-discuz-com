# Discuz! X3.4 转换 Discuz! Q

<h2>操作场景</h2> 

本文档将指导您如何将 Discuz! X3.4 应用程序数据迁移至Discuz! Q 应用程序，通过该操作可以将 Discuz! X3.4 中的用户、版块、普通主题、回复、附件图片等数据转换至 Discuz! Q。

::: danger
- 在转换程序开始前，为确保因操作失误，导致数据丢失等问题，请提前做好数据备份。
- 在转换程序开始前，请保证 Discuz! Q 数据库为全新安装，或者只通过本操作提供的转换程序转换过数据。否则将可能会导致数据丢失，或转换错误。
- 转换程序工具需要在 PHP 7.2.5 及以上环境运行，请确保已配置该环境。建议在部署的 Discuz! Q 站点运行。本文档以在部署的 Discuz! Q 的服务器上进行转化为例。
- Discuz! Q 转换工具仅支持 Discuz! X3.4 转 Discuz! Q 2.0 版本，不支持 Discuz! Q 3.0。若您需使用 Discuz! Q 3.0 以上版本，建议您安装部署 Discuz! Q 2.0 版本转换后在[升级为 Discuz! Q 3.0 以上版本](https://discuz.com/docs/%E5%B8%B8%E8%A7%84%E9%83%A8%E7%BD%B2%E5%8D%87%E7%BA%A7.html)。Discuz! Q 2.0 版本建议使用 v0.3.200315 进行转换，兼容性更高。
- 安装指定 Discuz! Q 版本详情参见：[Discuz! Q | 常见问题 QA 及其他](https://discuz.chat/thread/48991)。
:::


<h2>前提条件</h2> 

- 已部署 Discuz! Q。
- 具备 PHP 7.2.5 及以上环境的主机。

<h2>操作步骤</h2> 

### 步骤一：下载并解压转化工具

1. 您可以 [单击此处](https://discuzq-docs-1258344699.cos.ap-guangzhou.myqcloud.com/convert/xconvertq.zip) 下载转化工具至 已部署的 Discuz! Q 的服务器上。
::: tip
如果您是 Linux 服务器，您可以在服务器的终端中，通过该命令进行下载 `wget -c https://discuzq-docs-1258344699.cos.ap-guangzhou.myqcloud.com/convert/xconvertq.zip`。
:::
2. 使用解压工具解压压缩包。

::: tip
如果您是 Linux 服务器，您可以在服务器的终端中，通过该命令进行解压 `unzip xconvertq.zip`。
:::

### 步骤二：配置转化工具

#### 配置转化工具数据库信息
1. 在解压出的 `xconvertq` 文件夹中，找到 `config` 目录下的`database.php`文件，并使用文本编辑器或使用 WinSCP 等工具打开该文件。

2. 在 `database.php` 文件中配置数据库信息。信息内容可根据文件中注释进行填写。如下所示：

```mysql
<?php

return [
    'discuzq' => [
        'driver'    => 'mysql',
        'host'      => 'localhost', //Q数据库地址
        'port' => '3306', //Q数据库端口
        'prefix'    => 'pre_', //Q表前缀，没有则留空
        'database'  => '', //Q数据库名
        'username'  => '', //Q数据库用户
        'password'  => '', //Q数据库密码
        'charset'   => 'utf8', //Q数据编码
        'collation' => 'utf8mb4_unicode_ci', //Q数据库字符集
    ],
    'discuzx' => [
        'driver'    => 'mysql',
        'host'      => 'localhost', //X数据库地址
        'port' => '3306', //X数据库端口
        'prefix'    => 'pre_', //X表前缀，没有则留空
        'database'  => '', //X数据库名
        'username'  => '', //X数据库用户
        'password'  => '', //X数据库密码
        'charset'   => 'utf8', //X数据编码
        'collation' => 'utf8_unicode_ci', //X数据库字符集
    ]
];
```

:::tip
- 其中 `discuzq` 为 `Discuz! Q` 数据库连接信息， `discuzx` 为 `Discuz! X3.4` 数据库连接信息。
- 若 `Discuz! X3.4` 为 `gbk` 编码，数据库连接信息中的`X数据编码` 和`X数据库字符集`保持现有默认配置即可。
::: 

3. 在终端中，在该程序的根目录下输入命令 `php discoa app:test` 测试数据库是否正常连接。提示 `success` 即代表连接正常，若提示其他信息，请检查数据库配置信息是否填写正确。

#### 配置转化工具转换模式
1. 在解压出的 `xconvertq` 文件夹中，找到`config`目录下的`config.php`文件，并使用文本编辑器或使用 WinSCP 等工具打开该文件。
2. 配置 `breakpoint_continuation`字段，可选值为 true 与 false。说明如下：
- 当 `breakpoint_continuation` 字段的值为 `true `时，表示开启断点续转模式，在此模式下执⾏转换命令，程序会忽略之前转换过的数据。
- 当 `breakpoint_continuation` 的值为 false 时，程序会先检查Q⽬标表是否有多余数据，如果有则停
⽌执⾏对应的转换任务

### 步骤三：配置 Discuz! Q 数据库

1. 在 Discuz! Q 使用的数据库中运⾏以下 sql 命令：

::: warning
请您注意修改以下 sql 命令中 `pre_users` 的表格前缀，如果没有前缀，去掉 pre_ 即可。
:::

```sql
ALTER TABLE `pre_users` ADD `salt` CHAR(6) NULL AFTER `password`;
ALTER TABLE `pre_posts` CHANGE `content` `content` MEDIUMTEXT CHARACTER SET
utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '内容';
```

### 步骤四：修改 Discuz! Q 文件
#### 更新 Discuz! Q 登陆验证⽂件 
1. 在 Discuz! Q  文件目录中，找到 `app/Listeners/User/CheckLogin.php` 文件，并使用文本编辑器或使用 WinSCP 等工具打开该文件。
2. 编辑 `CheckLogin.php` 文件，查找以下代码（72⾏附近）。
```php
if ($event->password !== '' && !$event->user->checkPassword($event->password)) {
```
3. 在以上代码前添加如下代码：
```php
$this->checkSaltPassword($event);
```

4. 在 `CheckLogin.php `程序⽂件最后⼀个⼤括号`}`前，给本⽂件添加如下代码（**注意：**复制粘贴后，换行处会多出空格，例如`->`会变成`- >`，请注意去除）：

```php
/**
     * @param $event
     * Discuz! X3.4 密码转换
     */
private function checkSaltPassword($event) {
  if (!empty($event->user->salt) && !empty($event->password)) {
    $passwordmd5 = preg_match('/^\w{32}$/', $event->password) ? $event->password : md5($event->password);
    $passwordmd5 = md5($passwordmd5 . $event->user->salt);
    if ($event->user->password == $passwordmd5) {
      $event->user->password = $event->password;
      $event->user->salt = '';
      $event->user->save();
    }
  }
}
```

5. 将转换程序⾥的⽂件 `app/Formatter/CustomerConfigurator.php` 复制到 Discuz! Q 程序
`app/Formatter`⽬录下，并在 Discuz! Q 程序⽂件 ` app/Formatter/Formatter.php` 中**38⾏**左右找到如下代码：
```php
parent::confTopic($configurator);
```

在其后添加如下代码：

```php
CustomerConfigurator::Customer($configurator);
```

6. 删除 Discuz! Q 程序`storage/Formatter` ⽬录下的所有缓存⽂件。

### 步骤五：上传附件文件
- 将 Discuz! X3.4 ⽬录 `uc_server/data/avatar` 中的所有⽂件上传⾄ Discuz! Q ⽬录`storage/app/public/avatars`文件夹下，如果 avatars 不存在，请⾃⾏创建该⽬录。
- 将 Discuz! X3.4 ⽬录 `data/attachment/forum` 中的所有⽂件上传⾄ Discuz! Q ⽬录`storage/app/public/attachments`，如果 `attachments` 不存在，请⾃⾏创建该⽬录。
- 将 Discuz! X3.4 ⽬录 `data/attachment/commom` ⽂件夹上传⾄ Discuz! Q ⽬录 `storage/app/public/attachments`。
- 将 Discuz! X3.4 ⽬录 `static/image/smiley` ⽂件夹中的⽂件夹上传⾄ Discuz! Q ⽬录 `public/emoji`⽬录。

### 步骤六：转换
1. 转换工具支持以下两种方式进行转化，您可以根据自己的实际需求进选择：
#### ⼀键转换
在命令⾏模式下，在转换程序根⽬录运⾏ `php discoa app:xtq` 命令，将完成所有本程序定义过的数据的转
换，包含：⽤户数据、版块数据、主题数据、回复数据、附件数据、表情数据。

#### 选择转换

::: danger
若采⽤选择转换，⽤户数据和版块信息⼀定要在主题、回复信息之前进⾏操作。
::: 
您可以⾃由选择要转换的数据。您可以使⽤ `php discoa app:xtq --option` 命令，⾃由选择您需要转换的
数据，`--option` 有以下可使⽤参数：

- user : ⽤户信息转换。
- category : 版块信息转换。
- thread ： 主题信息转换。
- post ： 回复信息转换。
- attachment ： 附件信息转换。
- emoji ： 表情转换。
- count ：主题、帖⼦、⽤户信息统计更新。

::: tip
- `php discoa app:xtq user` 表示只转换⽤户信息。
- ` php discoa app:xtq thread` 表示只转换主题信息。
::: 

2. 转换工具命令运行完毕后即可转换成功。转化工具还支持以下功能。您可以根据您得具体需求进行使用。


#### 转化工具辅助功能

您可以使⽤ `php discoa app:clean cleanDatabase --option` 命令清理 Discuz! Q 中数据表数据。您可以
选择以下 `--option` 参数：

- user : 清理⽤户数据。
- userWallet : 清理⽤户钱包数据。
- category : 清理板块。
- thread ： 清理主题。
- post ： 清理回帖。
- emoji: 清理转换来的表情。
- attachment ： 清理附件数据。

::: warning
不填写 `option` 参数时，程序将清理以上所有数据。
::: 

例如：
- `php discoa app:clean cleanDatabase` 表示清理所有数据。
- `php discoa app:clean cleanDatabase user` 表示清理⽤户数据。

### 步骤七：Discuz! X3.4 网址跳转 Discuz! Q 

::: danger
- 转换成功后，将导致原 Discuz! X3.4 的网址失效。您可通过以下步骤与配置，让原 Discuz! X3.4 的网址被访问时，可顺利的利用 301 跳转至对应的 Discuz! Q 网址。
- 此步骤非必须，您可根据您的具体需求判断是否配置。
:::

目前提供以下三种 Web 服务的配置方式，您可根据您的具体情况进行选择配置：

#### Nginx

1. 使用文本编辑器打开您的 Nginx 配置文件，通常为 `nginx.conf`文件。
2. 在配置文件中查找到 `server` 块并增加如下配置：

```nginx
# 帖子列表
rewrite ^/forum-(\d+)-\d+\.html /category/$1? redirect;
if ($request_uri ~* ^/forum\.php\?mod=forumdisplay&fid=(\d+)) {
    set $fid $1;
    rewrite ^/ /category/$fid? redirect;
}

# 帖子详情
rewrite ^/thread-(\d+)-\d+-\d+\.html /thread/$1? redirect;
if ($request_uri ~* ^/forum\.php\?mod=viewthread&tid=(\d+)) {
    set $tid $1;
    rewrite ^/ /thread/$tid? redirect;
}

# 个人主页
rewrite ^/space-uid-(\d+)\.html /user/$1? redirect;
if ($request_uri ~* ^/home\.php\?mod=space&uid=(\d+)) {
    set $uid $1;
    rewrite ^/ /user/$uid? redirect;
}
```

3. 您可以使用以下 `nginx` 命令检查配置是否正确：

```nginx
nginx -t
```

4. 重启 Nginx 服务。您可以参考命令：`nginx -s reload` 进行重启。

#### Apache

1. 使用文本编辑器打开您的  `.htaccess`  配置文件并增加如下配置：

```
RewriteEngine On
# 帖子列表
RewriteRule ^/?forum-(\d+)-\d+\.html /category/$1? [R,L]
RewriteCond %{QUERY_STRING} mod=forumdisplay&fid=(\d+)
RewriteRule ^/?forum\.php /category/%1? [R,L]

# 帖子详情
RewriteRule ^/?thread-(\d+)-\d+-\d+\.html /thread/$1? [R,L]
RewriteCond %{QUERY_STRING} mod=viewthread&tid=(\d+)
RewriteRule ^/?forum\.php /thread/%1? [R,L]

# 个人主页
RewriteRule ^/?space-uid-(\d+)\.html /user/$1? [R,L]
RewriteCond %{QUERY_STRING} mod=space&uid=(\d+)
RewriteRule ^/?home\.php /user/%1? [R,L]
```

2. 重启 Apache 服务。您可以参考命令：`service httpd restart` 进行重启。

#### IIS

1. 使用文本编辑器打开您的  `web.config`  配置文件。
2. 在配置文件中的 `rules` 中增加如下配置：

```xml
<rule name="帖子列表伪静态" stopProcessing="true">
    <match url="^forum-(\d+)-\d+\.html" ignoreCase="false" />
    <action type="Redirect" url="/category/{R:1}" appendQueryString="false" redirectType="Found" />
</rule>
<rule name="帖子列表" stopProcessing="true">
    <match url="^forum\.php" ignoreCase="false" />
    <conditions logicalGrouping="MatchAll">
        <add input="{QUERY_STRING}" pattern="mod=forumdisplay&amp;fid=(\d+)" ignoreCase="false" />
    </conditions>
    <action type="Redirect" url="/category/{C:1}" appendQueryString="false" redirectType="Found" />
</rule>
<rule name="帖子详情伪静态" stopProcessing="true">
    <match url="^thread-(\d+)-\d+-\d+\.html" ignoreCase="false" />
    <action type="Redirect" url="/thread/{R:1}" appendQueryString="false" redirectType="Found" />
</rule>
<rule name="帖子详情" stopProcessing="true">
    <match url="^forum\.php" ignoreCase="false" />
    <conditions logicalGrouping="MatchAll">
        <add input="{QUERY_STRING}" pattern="mod=viewthread&amp;tid=(\d+)" ignoreCase="false" />
    </conditions>
    <action type="Redirect" url="/thread/{C:1}" appendQueryString="false" redirectType="Found" />
</rule>
<rule name="个人主页伪静态" stopProcessing="true">
    <match url="^space-uid-(\d+)\.html" ignoreCase="false" />
    <action type="Redirect" url="/user/{R:1}" appendQueryString="false" redirectType="Found" />
</rule>
<rule name="个人主页" stopProcessing="true">
    <match url="^home\.php" ignoreCase="false" />
    <conditions logicalGrouping="MatchAll">
        <add input="{QUERY_STRING}" pattern="mod=space&amp;uid=(\d+)" ignoreCase="false" />
    </conditions>
    <action type="Redirect" url="/user/{C:1}" appendQueryString="false" redirectType="Found" />
</rule>
```
3. 重启 IIS 服务。

### 其他注意事项

1.  目前附件图片不支持七牛云等第三方存储。

2. 若转换过程出现内存溢出报错`Fatal error: Allowed memory size of ...`时，可尝试修改`php.ini`配置，将`memory_limit`适当调大并重启 PHP 服务。