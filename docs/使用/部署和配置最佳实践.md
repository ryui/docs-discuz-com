#  部署和配置最佳实践

## 概述
本文档将对 Discuz! Q 典型应用场景下的最佳实践方式进行介绍与指导 ，在部署和配置 Discuz! Q 中，您可以参考本文档进行部署和配置。

## 部署
官方提供详细的 Discuz! Q 安装部署文档，您可以根据您的具体需求参考对应的安装部署指南进行部署。详情参见： [Discuz! Q 安装部署指南](https://discuz.com/docs/)。

## 开发
若您需对 Discuz! Q 进行二次开发，官方提供详细的 Discuz! Q 2. 二次开发部署
，您可以参考二次开发部署进行安装部署。详情参见： [二次开发部署](https://discuz.com/docs/%E4%BA%8C%E6%AC%A1%E5%BC%80%E5%8F%91%E9%83%A8%E7%BD%B2.html)。


## 管理端配置
:::tip
以下推荐配置相对较严格，请根据您的实际需求进行选择配置。
:::

### 开启验证码
在 Discuz! Q 管理后台中，依次单击【全局】>【腾讯云设置】>【验证码】，开启腾讯云验证码服务。详细操作参见 Discuz! Q 后台使用手册： [验证码](https://discuz.com/manual-admin/2.html#_2-7-%E8%85%BE%E8%AE%AF%E4%BA%91%E8%AE%BE%E7%BD%AE)。

![](https://main.qcloudimg.com/raw/ba86ab072b9c8f6a4143d7222b8b1a09.png)

### 注册登录安全配置
在 Discuz! Q 管理后台中，进行以下操作：
- 在【全局】 ->【注册与登录设置】 处勾选【新用户注册审核】。
- 在【全局】 -> 【注册与登录设置】处勾选【新用户注册启用验证码】。
- 在【全局】 ->【注册与登录设置】-> 【注册密码最小长度】处，输入10 或10以上数字，设置注册密码长度。
- 在【全局】->【注册与登录设置】 -> 【密码字符类型】处，勾选3种或3种类型以上。

![](https://main.qcloudimg.com/raw/6181bf6fe1f1ab95543c3f1a165c9e1c.png)

### 内容发布安全配置
在 Discuz 管理后台中，开启以下服务：
- 在【全局】 -> 【腾讯云设置】中，开启【实名认证】服务。详细操作参见 Discuz! Q 后台使用手册： [实名认证](https://discuz.com/manual-admin/2.html#_2-7-5-%E5%AE%9E%E5%90%8D%E8%AE%A4%E8%AF%81)。
- 在【全局】 -> 【腾讯云设置】中，开启【短信】服务。详细操作参见 Discuz! Q 后台使用手册： [短信](https://discuz.com/manual-admin/2.html#_2-7-4-%E7%9F%AD%E4%BF%A1)。

![](https://main.qcloudimg.com/raw/a7d31358c59e8f3eb589025c6d911948.png)



- 在【全局】 -> 【腾讯云设置】中，开启【图片内容安全】。详细操作参见 Discuz! Q 后台使用手册： [图片内容安全](https://discuz.com/manual-admin/2.html#_2-7-%E8%85%BE%E8%AE%AF%E4%BA%91%E8%AE%BE%E7%BD%AE)。
- 在【全局】 -> 【腾讯云设置】中，开启【文本内容安全】。详细操作参见 Discuz! Q 后台使用手册： [文本内容安全](https://discuz.com/manual-admin/2.html#_2-7-%E8%85%BE%E8%AE%AF%E4%BA%91%E8%AE%BE%E7%BD%AE)。

![](https://main.qcloudimg.com/raw/0a7c68a8e41425907a4d53ae25e9d0b3.png)

【全局】-> 【内容过滤设置】，按需自定义配置。

并在 Discuz! Q 管理后台中，进行以下操作：
- 在【用户】->【用户角色】->【所有角色的设置】->【短信】->【安全设置】下，勾选发布相关的“验证码”、“实名认证”、“绑定手机” 。如下图所示：

![](https://main.qcloudimg.com/raw/176ebf48d7d95d65846d7cdb05bebeb8.png)


### 	其它补充说明
若同时使用**小程序**和 **H5**，请务必在 [微信开发平台](open.weixin.qq.com)绑定公众号和小程序，否则会出现访问小程序和H5时，同一个微信号产生不同的账号问题，详细配置方法，请参见：[第三方登录设置](https://discuz.com/manual-admin/2.html#_2-3-%E7%AC%AC%E4%B8%89%E6%96%B9%E7%99%BB%E5%BD%95%E8%AE%BE%E7%BD%AE)。


## 安全配置
### 网络安全配置
- 接入腾讯云 [DDoS 防护](https://cloud.tencent.com/product/ddos) ，详情请参见 [DDoS 防护](https://cloud.tencent.com/document/product/1020) 文档。
- 接入腾讯云[Web 应用防火墙](https://cloud.tencent.com/product/waf)，详情请参见 [Web 应用防火墙](https://cloud.tencent.com/document/product/627) 文档。


### 主机安全配置
- 若您使用腾讯云云服务器 CVM 部署，建议您进行安全组设置，服务器端口只放通 80/443 端口，不放通其他端口。详情请参见：[配置安全组](https://cloud.tencent.com/document/product/213/15377)。
- 禁止可执行脚本上传至服务器。
- Discuz! Q 进程运行用户禁止使用 root 权限。


###  账户安全配置
- admin 管理端不对外网展示，外网限制 ip 白名单访问。
-  管理员密码禁止为弱口令。
-  权限组配置非管理员用户禁止赋予编辑及高级权限。

###  内容安全配置
- 内容审核接入腾讯云[文本内容安全](https://cloud.tencent.com/product/tms) 。
- 内容防刷接入腾讯云[验证码](https://cloud.tencent.com/product/captcha)。

### 代码安全
- 二次开发完成后可采用代码安全扫描工具进行安全扫描后在进行部署。
- 禁止或尽量避免代码中使用不可控的 shell 脚本执行函数。

### 数据安全
- 避免使用主机的自建数据库，建议使用 [云数据库](https://cloud.tencent.com/product/cdb) 进行部署。


## 站点加速配置
站点可根据需求接入[ 腾讯云内容分发网络 CDN](https://cloud.tencent.com/product/cdn) 进行站点加速。





