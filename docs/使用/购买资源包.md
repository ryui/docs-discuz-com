# 购买资源包
## 概述
本文指导您如何指导您如何购买[ 中小企业最佳部署实践](https://discuz.com/docs/DiscuzQ%20%E9%83%A8%E7%BD%B2%E6%9C%80%E4%BD%B3%E5%AE%9E%E8%B7%B5.html#%E6%A6%82%E8%BF%B0) 中资源包。

:::tip
以下资源包规格您可根据您的具体情况进行调整。
:::

## 操作指南

### 云托管资源包
1. 登录 [云开发环境控制台](https://console.cloud.tencent.com/tcb)。
2. 在云开发环境页中，单击【购买资源包】。

![](https://main.qcloudimg.com/raw/b3f5dfb8f41486cbdb8324d3e5ca17a2.png)

3. 在弹出的 “购买按量计费资源包” 窗口中，选择以下规格云资源包。如下图所示：
 - CPU：720 核*小时。
 - 内存：720 GB*小时。
 - 流量：30 GB。
 - 构建时长：450 Min。

![](https://main.qcloudimg.com/raw/501d2dca8270415379b3a7bb95040ef0.png)

:::tip
购买时长您可根据您的具体情况进行选择。
:::

4. 单击【立即购买】，完成支付流程后即可完成购买。


### 对象存储资源包
#### COS标准存储容量包、COS外网下行流量包
1. 登录并访问 [Discuz! Q 活动页](https://cloud.tencent.com/act/event/discuzq)。
2. 在购福利处，选择【COS标准存储容量】与 【COS外网下行流量】。

![](https://main.qcloudimg.com/raw/e917665dc8d5e6fba268d5452289f520.png)
3. 单击【立即购买】，完成支付流程后即可完成购买。

### 云点播资源包
#### 云点播体验包、云点播流量包、云点播转码包、云点播存储包 

1. 登录并访问 [Discuz! Q 活动页](https://cloud.tencent.com/act/event/discuzq)。
2. 在购福利处，选择您需要的点播资源包。

![](https://qcloudimg.tencent-cloud.cn/raw/487d6bdd538b2b05f16ef1e1781196aa.png)
3. 单击【立即购买】，完成支付流程后即可完成购买。


#### 标准存储请求包

1. 登录并访问 [对象存储(COS)资源包](https://buy.cloud.tencent.com/cos?packageType=std) 购买页。
2. 选择配置，如下图所示：
 - 计费项分类：读写请求。
 - 资源包类型：标准存储请求包。
 - 生效地域：中国大陆通用。
 - 规格：100万次。

![](https://main.qcloudimg.com/raw/4c1aca1a0dfc4fb24d0578a30cd05b5b.png)
:::tip
购买时长您可根据您的具体情况进行选择。
:::

3. 单击【立即购买】，完成支付流程后即可完成购买。


:::tip
若您还需其他云资源，您可直接在该页进行购买，相比产品购买页进行购买，更优惠。
:::