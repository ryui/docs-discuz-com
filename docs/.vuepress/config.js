module.exports = {
	title: 'Discuz! Q 官方文档',
	base: '/',
	evergreen: true,
	themeConfig: {
		lastUpdated: '最后更新',
		editLinks: false,
		smoothScroll: true,
		nav: [
			{ text: '主页', link: '/' },
			//{ text: 'API 文档', link: '/api-docs/' },
			{ text: '官网', link: 'https://discuz.com' },
			{ text: '项目地址', link: 'https://gitee.com/Discuz/Discuz-Q-Docs-New' },
			{ text: 'REST API v1', link: 'https://discuz.com/api-docs/v1' },
			{ text: '旧版文档', link: 'https://discuz.com/docs-v1/' },
		],
		sidebar: {
			'/api-docs/': [
				{
					title: '关于',
					sidebarDepth: 1,
					collapsable: true,
					children: ['联系我们'],
				},
			],

			'/': [
				{
					title: '安装',
					sidebarDepth: 1,
					collapsable: true,
					children: ['/安装/开源应用中心安装', '/安装/Windows 主机', '/安装/Linux 主机', '/安装/腾讯云安装','/安装/小程序'],
				},
				{
					title: '使用',
					sidebarDepth: 1,
					collapsable: true,
					children: ['/使用/初次使用配置说明', '/使用/部署最佳实践', '/使用/购买资源包', '/使用/部署和配置最佳实践', '/使用/后台使用手册'],
				},
                {
					title: '配置',
					sidebarDepth: 1,
					collapsable: true,
					children: ['/配置/云 API 设置', '/配置/图片内容安全设置', '/配置/文本内容安全设置', '/配置/短信设置', '/配置/实名认证设置', '/配置/对象存储设置', '/配置/云点播设置', '/配置/验证码设置', '/配置/CDN 设置', '/配置/其他服务设置'],
				},
				{
					title: '升级和转换',
					sidebarDepth: 1,
					collapsable: true,
					children: ['/升级/常规部署升级', '/升级/镜像部署方式升级','/升级/Docker 部署方式升级', '/升级/小程序的升级', '/升级/Discuzto', '/升级/本地数据迁移', '/升级/站点接入 CDN', '/升级/本地数据迁移'],
				},
				{
					title: '开发',
					sidebarDepth: 1,
					collapsable: true,
					children: ['/开发/开发说明', '/开发/目录结构', '/开发/API 说明', '/开发/二次开发部署', '/开发/构建说明', '/开发/API文档', '/开发/数据字典'],
				},
				{
					title: '关于',
					sidebarDepth: 1,
					collapsable: true,
					children: ['/关于/常见问题','/关于/联系我们', '/关于/更新记录' ],
				},
			],
			
		},
	},
	head: [
		[
			'script',
			{},
			`
      document.addEventListener("DOMContentLoaded", function(){
        console.log("ready");
        var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        if (window.location.hash && isChrome) {
            setTimeout(function () {
                var hash = window.location.hash;
                var element_to_scroll_to = document.getElementById(decodeURI(hash.substr(1)));
                element_to_scroll_to.scrollIntoView();
            }, 300);
        }
      });
`,
		],
	],
	plugins: [
		[
			'copy-code',
			{
				copyMessage: '已复制到剪贴板。', // default is 'Copy successfully and then paste it for use.'
				duration: 1000, // prompt message display time.
			},
		],
	],
};
