﻿# CDN 设置

## 概述
若您需使用腾讯云 CDN 服务加速您的 Discuz! Q 网站访问，您可参考本文档快速在  Discuz! Q 后台完成设置。

## 前提条件
- 开启该功能需要先 [开启云 API 设置](https://discuz.com/docs/%E4%BA%91%20API%20%E8%AE%BE%E7%BD%AE.html) 才可正常使用。
- 您的 Discuz! Q 站点解析域名需 [托管在 DNSPod](https://docs.dnspod.cn/dns/604f1da0b9640b6a785aa304/)。

:::tip
- 若您的解析域名未托管在 DNSPod，您可将您的 [解析域名转入 DNSPod](https://docs.dnspod.cn/dns/604f1da0b9640b6a785aa304/) 后在进行以下操作。
- 若您解析域名无法转入 DNSPod，您也可参考 [站点接入 CDN](https://discuz.com/docs/%E7%AB%99%E7%82%B9%E6%8E%A5%E5%85%A5%20CDN.html#%E6%A6%82%E8%BF%B0) 手动配置。
:::

## 操作指南
1. 登录您的 Discuz! Q 站点后台。
2. 在 **全局**->**腾讯云设置**->**CDN** 处单击 **配置**。如下图所示：

![](https://qcloudimg.tencent-cloud.cn/raw/3f4a32fdb6df8fe85b9a8986733225c7.png)

3. 在 CDN 配置页，填写配置信息。如下图所示：

![](https://qcloudimg.tencent-cloud.cn/raw/0cdf33fde80f600d4f06cfa70ee22508.png)

**加速域名**：默认情况输入您的 Discuz! Q 站点域名。如：`discuz.chat`。

:::tip
- 加速域名长度不超过81个字符。
- 加速域名需已经在工信部完成备案。
:::

**源站地址**：默认情况请填写域名绑定的 IP 地址，即服务器公网IP。

:::tip
-支持配置多个 IP 作为源站，回源时会进行轮询回源；
-支持增加配置端口（0 - 65535）和权重（1 - 100）：源站:端口:权重（端口可缺省：源站::权重），HTTPS 协议暂时仅支持443端口；
-支持配置域名作为源站，此域名需要与业务加速域名不一致。
:::

**回源HOST**：回源 HOST 是指 CDN 节点在回源时，在源站访问的站点域名，默认情况与加速域名相同。

**主域名**：您加速域名的主域名。如您的加速域名为`dzq.discuz.chat`，则填写`discuz.chat`。

4. 单击**提交**，页面提示“CDN配置成功”即可完成设置。



